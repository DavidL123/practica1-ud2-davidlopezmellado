/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.vehiculosbbdd.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Vista {
    private JPanel panel1;
     JTextField txtNombre;
     JTextField txtApellidos;
     JTextField txtDomicilio;
     JTextField txtDni;
     JTextField txtPrecioHabitacion;
     JTable table1;
     JButton btnBuscar;
     JButton btnNuevo;
     JButton btnEliminar;
     JTextField txtBuscar;
     DateTimePicker dateTimePicker1;
     DateTimePicker dateTimePicker2;
     DatePicker datePicker1;
    JLabel lblAccion;
    JButton btnOrdenar;
    JTable table2;


    DefaultTableModel dtm;
    DefaultTableModel dtm2;
     JMenuItem itemConectar;
     JMenuItem itemCrearTabla;
     JMenuItem itemSalir;
     JMenuItem itemImportarExcel;
     JMenuItem itemExportarExcel;
     JFrame frame;

    /**
     * Método constructor de clase Vista
     * Nos crea un JFrame, dos TableModel y les pone a los objetos JTable cual va a ser su TableModel (.setModel)
     */

    public Vista(){
         frame= new JFrame("Hotel BBDD");
         frame.setContentPane(panel1);
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

         dtm2= new DefaultTableModel();
         table2.setModel(dtm2);
         dtm= new DefaultTableModel();
         table1.setModel(dtm);

         crearMenu();

         frame.pack();
         frame.setVisible(true);
     }

    /**
     * Método que nos creará un menú desplegable en la barra y nos añadirá distintos items de menú.
     */
    private void crearMenu(){
         itemConectar= new JMenuItem("Conectar");
         itemConectar.setActionCommand("Conectar");
         itemCrearTabla = new JMenuItem("Crear tabla clientes");
         itemCrearTabla.setActionCommand("CrearTablaClientes");
         itemImportarExcel=new JMenuItem("Importar tabla clientes");
         itemImportarExcel.setActionCommand("ImportarTablaClientes");
         itemExportarExcel=new JMenuItem("Exportar tabla clientes");
         itemExportarExcel.setActionCommand("ExportarTablaClientes");
         itemSalir=new JMenuItem("Salir");
         itemSalir.setActionCommand("Salir");



         JMenu menuArchivo = new JMenu ("Archivo");
         menuArchivo.add(itemConectar);
         menuArchivo.add(itemCrearTabla);

         menuArchivo.add(itemImportarExcel);
         menuArchivo.add(itemSalir);
         //menuArchivo.add(itemExportarExcel);

         JMenuBar barraMenu = new JMenuBar();
         barraMenu.add(menuArchivo);

         frame.setJMenuBar(barraMenu);
     }
}
