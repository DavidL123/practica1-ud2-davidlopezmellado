/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.vehiculosbbdd.gui;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Controlador implements ActionListener, TableModelListener {
    
    private Vista vista;
    private Modelo modelo;

    private File ficheroImportar;
    private File ficheroExportar;

    private enum tipoEstado{conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor de la clase Controlador
     * Recibe los objetos de las clases Vista y Modelo
     * Ejecuta los métodos iniciarTabla() e iniciarTabla2()
     *
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo){
        this.modelo=modelo;
        this.vista=vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        iniciarTabla2();
        addActionListener(this);
        addTableModelListener(this);

    }

    /**
     * Método que nos configura los listener para poder recibir cuando alguien pulse algunos de los botones.
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnOrdenar.addActionListener(listener);
        vista.itemImportarExcel.addActionListener(listener);
        /*
        vista.itemImportarExcel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
*/

        /*
        vista.itemExportarExcel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if(opcion == JFileChooser.APPROVE_OPTION){
                    ficheroExportar = selectorArchivo.getSelectedFile();

                }
            }
        });
        */
    }

    /**
     * Método que nos permitirá recoger la información sobre cambios hechos en los TableModel
     *
     * @param listener
     */
    private void addTableModelListener(TableModelListener listener) {
        vista.dtm.addTableModelListener(listener);
        vista.dtm2.addTableModelListener(listener);
    }


    /**
     * Método que nos permitirá efectuar los cambios que se realicen en el TableModel dtm
     * @param e
     */

    @Override
    public void tableChanged(TableModelEvent e) {
        if(e.getType() ==TableModelEvent.UPDATE){

            int filaModificada=e.getFirstRow();

            try{
                modelo.modificarCliente((Integer)vista.dtm.getValueAt(filaModificada,0),
                        (String)vista.dtm.getValueAt(filaModificada,1),
                        (String)vista.dtm.getValueAt(filaModificada,2),
                        (java.sql.Date)vista.dtm.getValueAt(filaModificada,3),
                        (String)vista.dtm.getValueAt(filaModificada,4),
                        (String)vista.dtm.getValueAt(filaModificada,5),
                        (String.valueOf(vista.dtm.getValueAt(filaModificada,6))),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModificada, 7),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModificada, 8));
                vista.lblAccion.setText("Columna actulizada");

            }catch (SQLException e1){
                e1.printStackTrace();
            }
        }
    }

    /**
     * Método que nos permitirá  ejecutar una serie de órdenes a través de un switch que estará compuesto
     * por los botones que hemos seleccionado.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      String comando = e.getActionCommand();

      switch (comando){
          case "CrearTablaClientes":
              try{
                  modelo.crearTablaClientes();
                  vista.lblAccion.setText("Tabla clientes creada");
              }catch (SQLException e1){
                  e1.printStackTrace();
              }

              break;
          case "Nuevo":
              try{
                modelo.insertarCliente(vista.txtNombre.getText(),vista.txtApellidos.getText(),vista.datePicker1.getDate(),
                        vista.txtDomicilio.getText(),vista.txtDni.getText(),vista.txtPrecioHabitacion.getText(),
                        vista.dateTimePicker1.getDateTimePermissive(),vista.dateTimePicker2.getDateTimePermissive());
                //limpiarTabla(vista.table1);
                cargarFilas(modelo.obtenerDatos());
              }catch (SQLException e1){
                  e1.printStackTrace();
              }
              break;
          case "Buscar por nombre":

              try {

                  cargarFilas(modelo.buscarPorNombre(vista.txtBuscar.getText()));
              } catch (SQLException ex) {
                  ex.printStackTrace();
              }


              break;
          case "Eliminar":
              try{
                  int filaBorrar=vista.table1.getSelectedRow();
                  int idBorrar=(Integer) vista.dtm.getValueAt(filaBorrar,0);
                  modelo.eliminarCliente(idBorrar);
                  vista.dtm.removeRow(filaBorrar);
                  vista.lblAccion.setText("Fila Eliminada");
              }catch (SQLException e1){
                  e1.printStackTrace();
              }
              break;
          case "Salir":
              System.exit(0);
              break;

          case "Conectar":
              if(estado== tipoEstado.desconectado){
                  try{
                      modelo.conectar();
                      vista.itemConectar.setText("Desconectar");
                      estado=tipoEstado.conectado;
                      cargarFilas(modelo.obtenerDatos());
                  }catch (SQLException e1){
                      /*
                      JOptionPane.showMessageDialog(null, "Error de conexión",
                              "Error",JOptionPane.ERROR_MESSAGE);

                      try {
                          modelo.conectar();
                          vista.itemConectar.setText("Desconectar");
                          estado=tipoEstado.conectado;

                      } catch (SQLException ex) {
                          JOptionPane.showMessageDialog(null, "Error de conexión",
                                  "Error",JOptionPane.ERROR_MESSAGE);
                          ex.printStackTrace();
                      }
                        */
                      e1.printStackTrace();
                  }
                  /*
                  try {
                      cargarFilas(modelo.obtenerDatos());
                  } catch (SQLException ex) {
                      ex.printStackTrace();
                  }
                  */

              }else{
                  try{
                      modelo.desconectar();
                      vista.itemConectar.setText("Conectar");
                      estado=tipoEstado.desconectado;
                      vista.lblAccion.setText("Desconectado");
                  }catch (SQLException e1){
                      JOptionPane.showMessageDialog(null,"Error de desconexión",
                              "Error",JOptionPane.ERROR_MESSAGE);
                  }
              }
              break;
          case "Ordenar":

              try {

                  cargarFilas2(modelo.OrdenarPorFecha());
              } catch (SQLException ex) {
                  ex.printStackTrace();
              }
              break;

          case "ImportarTablaClientes":

              try {
                  JFileChooser selectorArchivo = new JFileChooser();
                  int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                  if(opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                      ficheroImportar = selectorArchivo.getSelectedFile();

                  }
                  modelo.ImportarArchivoExcel(ficheroImportar);
                  cargarFilas(modelo.obtenerDatos());
              } catch (SQLException ex) {
                  ex.printStackTrace();
              }
              break;
      }
    }


    /**
    Método que nos podrá los nombres de las columnas en el TablaModel dtm2
     */
    private void iniciarTabla2(){
        String [] headers={"Id","Nombre","Apellidos","Fecha final reserva"};
        vista.dtm2.setColumnIdentifiers(headers);
    }

    /**
     * Método que recibe un objeto ResultSet y nos carga cada elemento del ResultSet en un objeto de la clase Object
     * y nos lo añade a la clase addRow().
     * @param resultSet Objeto de la clase ResultSet
     * @throws SQLException Tipo de excepción.
     */

    private void cargarFilas2(ResultSet resultSet) throws SQLException{

        if(resultSet!=null) {
            Object[] fila = new Object[4];
            vista.dtm2.setRowCount(0);

            while (resultSet.next()) {
                fila[0] = resultSet.getObject(1);
                fila[1] = resultSet.getObject(2);
                fila[2] = resultSet.getObject(3);
                fila[3] = resultSet.getObject(4);

                vista.dtm2.addRow(fila);
            }
        }
    }


    /**
     Método que nos podrá los nombres de las columnas en el TablaModel dtm1
     */
    private void iniciarTabla(){
        String[] headers={"Id","Nombre","Apellidos", "Fecha nacimiento", "Domicilio", "Dni", "Precio habitacion reservada",
        "Fecha inicio reserva", "Fecha final reserva"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Método que recibe un objeto ResultSet y nos carga cada elemento del ResultSet en un objeto de la clase Object
     * y nos lo añade a la clase addRow().
     * @param resultSet Objeto de la clase ResultSet
     * @throws SQLException Tipo de excepción.
     */

    private void cargarFilas(ResultSet resultSet) throws SQLException{

        if(resultSet!=null) {
            Object[] fila = new Object[9];
            vista.dtm.setRowCount(0);

            while (resultSet.next()) {
                fila[0] = resultSet.getObject(1);
                fila[1] = resultSet.getObject(2);
                fila[2] = resultSet.getObject(3);
                fila[3] = resultSet.getObject(4);
                fila[4] = resultSet.getObject(5);
                fila[5] = resultSet.getObject(6);
                fila[6] = resultSet.getObject(7);
                fila[7] = resultSet.getObject(8);
                fila[8] = resultSet.getObject(9);

                vista.dtm.addRow(fila);
            }

            if (resultSet.last()) {
                vista.lblAccion.setVisible(true);
                vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
            }
        }

    }


    /*
    public void limpiarTabla(JTable tabla){
        try {
            DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
            int filas=tabla.getRowCount();
            for (int i = 0;i<filas; i++) {
                modelo.removeRow(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
        }
    }
    */







}
