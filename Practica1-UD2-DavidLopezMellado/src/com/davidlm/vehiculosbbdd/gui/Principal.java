/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.vehiculosbbdd.gui;

public class Principal {
    /**
     * Método main que nos crea objetos de las clases Vista, Modelo y Controlador.
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
