/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.vehiculosbbdd.gui;

import com.mysql.jdbc.SQLError;
import com.sun.org.apache.regexp.internal.RESyntaxException;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import javax.swing.*;
import java.io.File;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Modelo {


    private Connection conexion;


    /**
     * Método que nos conectará con la base de datos y ejecutará el procedimiento crearTablaCLientes()
     * @throws SQLException
     */
    public void crearTablaClientes() throws SQLException{
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/hotel1","root","");

        String sentenciaSql="call crearTablaClientes()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Método que nos conectará con la base de datos.
     * @throws SQLException
     */
    public void conectar() throws SQLException{
        conexion=null;
        conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/hotel1","root", "");

    }

    /**
     * Método que nos desconecta de la base de datos.
     * @throws SQLException
     */
    public void desconectar() throws SQLException{
        conexion.close();
        conexion=null;
    }

    /**
     * Método que nos permitirá hacer una consulta, guardará los datos en un objeto ResultSet y nos lo devolverá.
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException{
        if(conexion==null){
            return null;
        }
        if(conexion.isClosed()){
            return null;
        }
        ResultSet resultado=null;
        try {
            String consulta = "SELECT * FROM clientes";
            PreparedStatement sentencia = null;
            sentencia = conexion.prepareStatement(consulta);
            resultado = sentencia.executeQuery();
            return resultado;
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Probablemente no existe la tabla, ímportela, por favor",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        return resultado;
    }

    /**
     * Método que recibirá una serie de parámetros y nos permitirá ejecutar la sentencia de abajo,
     * que nos crea una nuevo cliente.
     *
     * @param nombre Nombre del cliente
     * @param apellidos Apellidos del cliente
     * @param fecha_nacimiento Fecha de nacimiento del cliente
     * @param domicilio Domicilio del cliente
     * @param dni Dni del cliente
     * @param precioHabitacionReservada Precio habitación reservada del cliente
     * @param fecha_inicio_reserva Fecha de inicio de reserva del cliente
     * @param fecha_fin_reserva Fecha de fin de reserva del cliente
     * @return
     * @throws SQLException
     */
    public int insertarCliente(String nombre, String apellidos, LocalDate fecha_nacimiento,
                                String domicilio, String dni, String precioHabitacionReservada,
                                LocalDateTime fecha_inicio_reserva, LocalDateTime fecha_fin_reserva) throws SQLException {
        if(conexion==null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }
        String consulta="INSERT INTO `clientes`(nombre,apellidos, fecha_nacimiento, domicilio, dni, precioHabitacionReservada, " +
                "fecha_inicio_reserva,fecha_fin_reserva) VALUES(?,?,?,?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidos);
        sentencia.setDate(3, Date.valueOf(fecha_nacimiento));
        sentencia.setString(4,domicilio);
        sentencia.setString(5,dni);
        sentencia.setDouble(6, Double.parseDouble(precioHabitacionReservada));
        sentencia.setTimestamp(7, Timestamp.valueOf(fecha_inicio_reserva));
        sentencia.setTimestamp(8, Timestamp.valueOf(fecha_fin_reserva));

        int numeroRegistros=sentencia.executeUpdate();

        if(sentencia!=null){
            sentencia.close();
        }

        return numeroRegistros;

    }

    /**
     * Método que recibirá un id y nos ejecutará una sentencia para eliminar el cliente deseado.
     * @param id Id del cliente
     * @return
     * @throws SQLException
     */
    public int eliminarCliente(int id) throws SQLException{
        if(conexion==null){
            return -1;
        }
        if(conexion.isClosed()){
            return -2;
        }

        String consulta="DELETE FROM clientes WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado=sentencia.executeUpdate();
        if(sentencia!=null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     *
     * Método que recibe una serie de parámetros y nos permite modificar un cliente a través de una sentencia que ejecuta.
     *
     * @param id Id del cliente
     * @param nombre Nombre del cliente
     * @param apellidos Apellidos del cliente
     * @param fecha_nacimiento Fecha de nacimiento del cliente
     * @param domicilio Domicilio del cliente
     * @param dni DNI del cliente
     * @param precioHabitacionReservada Precio habitación reservada del cliente
     * @param fecha_inicio_reserva Fecha de inicio reservada del cliente
     * @param fecha_fin_reserva Fecha de fin del cliente
     * @return
     * @throws SQLException
     */

    public int modificarCliente(int id, String nombre, String apellidos, Date fecha_nacimiento,
                                String domicilio, String dni, String precioHabitacionReservada,
                                Timestamp fecha_inicio_reserva, Timestamp fecha_fin_reserva) throws SQLException{
        if(conexion==null){
            return -1;
        }
        if(conexion.isClosed()){
            return -2;
        }

        String consulta= "UPDATE clientes SET nombre=?, apellidos=?, fecha_nacimiento=?," +
                "domicilio=?,dni=?,precioHabitacionReservada=?,fecha_inicio_reserva=?,fecha_fin_reserva=?" +
                "WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1, nombre);
        sentencia.setString(2,apellidos);
        sentencia.setDate(3,fecha_nacimiento);
        sentencia.setString(4, domicilio);
        sentencia.setString(5,dni);
        sentencia.setDouble(6,Double.parseDouble(precioHabitacionReservada));
        sentencia.setTimestamp(7,fecha_inicio_reserva);
        sentencia.setTimestamp(8,fecha_fin_reserva);
        sentencia.setInt(9,id);

        int resultado=sentencia.executeUpdate();

        if(sentencia!=null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Método que recibe el nombre buscado y nos permitirá buscar si existe a través de una consulta que se ejecutará
     * @param nombreBuscado Nombre buscado del cliente
     * @return
     * @throws SQLException
     */
    public ResultSet buscarPorNombre(String nombreBuscado) throws SQLException{
        if(conexion==null){
            return null;
        }
        if(conexion.isClosed()){
            return null;
        }

        String consulta ="SELECT * FROM clientes WHERE nombre=?";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1,nombreBuscado);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    public ResultSet OrdenarPorFecha() throws SQLException {
        if(conexion==null){
            return null;
        }
        if(conexion.isClosed()){
            return null;
        }

        String consulta="SELECT id,nombre,apellidos, fecha_fin_reserva FROM clientes ORDER BY fecha_fin_reserva ASC";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        ResultSet resultado=sentencia.executeQuery();


        return resultado;
    }


    /**
     * Método que recibirá un objeto de tipo fichero que nos permitirá ejecutar una sentencia que nos
     * cargará los datos de un archivo .csv
     * @param fichero Objeto de File
     * @return
     * @throws SQLException
     */

    public int ImportarArchivoExcel(File fichero) throws SQLException {
        if(conexion==null){
            return -1;
        }
        if(conexion.isClosed()){
            return -2;
        }

        //System.out.println(fichero.getAbsolutePath());

        String ruta=fichero.getAbsolutePath();

        String elResto="";
        String [] rutaVector= ruta.split("");
        String rutaBien="";
        for(int i=0;i<rutaVector.length;i++){
            if(rutaVector[i].equalsIgnoreCase("\\")){

                rutaBien=rutaBien+"\\\\";

            }else{
                rutaBien=rutaBien+rutaVector[i];
            }
        }
       // System.out.println(rutaBien);

        /*
        String consulta="LOAD DATA LOCAL INFILE '"+rutaBien+ "' INTO TABLE clientes CHARACTER SET latin2 " +
                "FIELDS TERMINATED BY ';' ENCLOSED BY '\"' LINES TERMINATED BY '\\n';";
         */
        String consulta="LOAD DATA  LOCAL INFILE '"+rutaBien+"' INTO TABLE clientes CHARACTER SET latin1" +
                " FIELDS TERMINATED BY ';' ENCLOSED BY '\"' LINES TERMINATED BY '\\n'";

        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);

        int resultado=sentencia.executeUpdate();


        if(sentencia!=null){
            sentencia.close();
        }


        return resultado;

    }
}
