/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */

 /**
   Nos crea una base de datos y la única tabla que contiene con sus correspondientes apartados.
  */
CREATE DATABASE hotel1;

USE hotel1;

CREATE TABLE clientes(
	id int primary key auto_increment,
	nombre varchar(30) unique,
	apellidos varchar(30),
	fecha_nacimiento date,
	domicilio varchar(40),
	dni varchar(40),
	precioHabitacionReservada float(30),
	fecha_inicio_reserva timestamp,
	fecha_fin_reserva timestamp


);



/**
  Procedimiento para crear la tabla clientes con sus correspondientes apartados.
 */

DELIMITER //
CREATE PROCEDURE crearTablaClientes()
BEGIN
	CREATE TABLE clientes(
				id int primary key auto_increment,
				nombre varchar(30) unique,
				apellidos varchar(30),
				fecha_nacimiento date,
				domicilio varchar(30),
				dni varchar(30),
				precioHabitacionReservada float(30),
				fecha_inicio_reserva timestamp,
				fecha_fin_reserva timestamp);
END //